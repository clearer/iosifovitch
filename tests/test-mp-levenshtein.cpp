#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <iosifovitch-parallel.hpp>
#include <iosifovitch.hpp>

/*
    This might as well just generate a set of random strings and verify
    that it agrees with the single threaded version.

    Performance on the test is not really required -- just use strings
    that are exactly long enough, to be interesting, to verify that
    things work.

    Consider having a bunch of very long strings with the distance
    pre-calculated by the single threaded version.
*/

auto random_string() -> std::string
{
    auto size = rand() % 100 + 1;
    std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
    std::string s;
    s.resize(size);
    std::transform(std::begin(s), std::end(s), std::begin(s), [alphabet](int) -> char { return alphabet[rand() % alphabet.size()];});
    return s;
}

TEST_CASE("Random strings match with single threaded version")
{
    for (auto i = 0; i < 100; ++i)
    {
        auto r1 = random_string();
        auto r2 = random_string();
        auto parallel_difference = iosifovitch::parallel::levenshtein_distance(r1, r2);
        auto single_threaded_difference = iosifovitch::levenshtein_distance(r1, r2);
        CHECK_EQ(parallel_difference, single_threaded_difference);
    }
}

TEST_CASE("Identical strings have distance 0")
{
	for (auto a : {"hello", "world", ""})
	{
		CHECK_EQ(iosifovitch::parallel::levenshtein_distance(a,a), 0);
	}
}

TEST_CASE("The distance to an empty string, is the length of the other string")
{
	for (std::string a : {"hello", "world", ""})
	{
		CHECK_EQ(iosifovitch::parallel::levenshtein_distance(a,""), a.length());
	}
}

TEST_CASE("Distance from \"aaaa\" to \"aaba\" is 1")
{
	CHECK_EQ(iosifovitch::parallel::levenshtein_distance("aaaa", "aaba"), 1);
	CHECK_EQ(iosifovitch::parallel::levenshtein_distance("aaba", "aaaa"), 1);
}


TEST_CASE("Distance from \"baba\" to \"aaaa\" is 2")
{
	CHECK_EQ(iosifovitch::parallel::levenshtein_distance("baba", "aaaa"), 2);
	CHECK_EQ(iosifovitch::parallel::levenshtein_distance("aaaa", "baba"), 2);
}


#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>
#include <iosifovitch.hpp>

TEST_CASE("Identical strings have distance 0")
{
	for (auto a : {"hello", "world", ""})
	{
		CHECK_EQ(iosifovitch::levenshtein_distance(a,a), 0);
	}
}

TEST_CASE("The distance to an empty string, is the length of the other string")
{
	for (std::string a : {"hello", "world", ""})
	{
		CHECK_EQ(iosifovitch::levenshtein_distance(a,""), a.length());
	}
}

TEST_CASE("Distance from \"aaaa\" to \"aaba\" is 1")
{
	CHECK_EQ(iosifovitch::levenshtein_distance("aaaa", "aaba"), 1);
	CHECK_EQ(iosifovitch::levenshtein_distance("aaba", "aaaa"), 1);
}


TEST_CASE("Distance from \"baba\" to \"aaaa\" is 2")
{
	CHECK_EQ(iosifovitch::levenshtein_distance("baba", "aaaa"), 2);
	CHECK_EQ(iosifovitch::levenshtein_distance("aaaa", "baba"), 2);
}



TEST_CASE("Distance from \"aaaa\" to \"aa\" is 2")
{
	CHECK_EQ(iosifovitch::levenshtein_distance("aaaa", "aa"), 2);
	CHECK_EQ(iosifovitch::levenshtein_distance("aa", "aaaa"), 2);
}



TEST_CASE("Distance from \"baaaab\" to \"aa\" is 4")
{
	CHECK_EQ(iosifovitch::levenshtein_distance("baaaab", "aa"), 4);
	CHECK_EQ(iosifovitch::levenshtein_distance("aa", "baaaab"), 4);
}


# Iosifovich -- an ultra fast implementation of the levenshtein distance function

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/865bbf23bbc84b9d816ab9fb25448169)](https://www.codacy.com?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=clearer/iosifovitch&amp;utm_campaign=Badge_Grade)

Pure and simple, Iosifovich aims to be the fastest implementation of the
levenshtein distance function on the planet.

# Migration

This project has been moved to https://gitea.gigo-games.dk/frederik/iosifovitch. All future development will happen here.

## What still needs to be done

*   SIMD implementations
*   Parallel implementations
*   GPGPU implementations
*   Further algorithm optimizations

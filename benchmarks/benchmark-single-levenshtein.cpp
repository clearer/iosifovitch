
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <sstream>
#include "iosifovitch.hpp"

int main(int argc, char ** argv)
{
    std::stringstream words; words <<
    #include "words"
    ;
    auto maximum_size = (argc == 2) ? std::stoi(argv[1]) : 10000ul;

    auto strings = std::vector<std::string>();
    strings.reserve(maximum_size);
    auto word = std::string();

    while(words && strings.size() < maximum_size){
        std::getline(words, word);
        strings.push_back(word);
    }
    std::cerr << "Total number of words: " << strings.size() << '\n';
	std::vector<size_t> buffer;

    for(std::string a : strings){
        for(std::string b : strings){
            iosifovitch::levenshtein_distance(a, b, buffer);
        }
    }

    std::cout << "Done.";
}
